const express = require('express');
const morgan = require('morgan');

const app = express();

app.use(morgan('dev'));

app.use(express.static('www'));

app.set('port', process.env.PORT || 5000);

app.get(/./, (request, response) => {
    response.redirect('/');
});

app.listen(app.get('port'));
