import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {

  public selectedIndex = 0;
  public selectedIndexPage = 0;
  public appPages = [
    {
      title: 'Cliente',
      url: 'Cliente/Peliculas',
      icon: 'people'
    },
    {
      title: 'Administrador',
      url: 'Administrador/Peliculas',
      icon: 'options'
    }
  ];

  public labels: object[];

  public plataforma = "Plataforma: " + this.platform.platforms();
  public location: string;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    this.location = window.location.pathname.split('/')[1];
    switch(this.location) {
      case 'Cliente':
        this.labels = [{
          title: 'Peliculas',
          icon: 'film-outline',
          url: 'Cliente/Peliculas'
        }];
        break;

      case 'Administrador':
        this.labels = [{
          title: 'Peliculas',
          icon: 'film-outline',
          url: 'Administrador/Peliculas'
        }];
        break;

        default:
          this.labels = [{
            title: 'Peliculas',
            icon: 'film-outline',
            url: 'Cliente/Peliculas'
          }];
          this.location = 'Cliente';
          this.selectedIndexPage = 0;
    }
    if (this.location !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === this.location.toLowerCase());
    }
  }

  getId(index: number) {
    this.selectedIndexPage = 0;
    switch (index) {
      case 0:
        this.location = 'Cliente';
        this.labels = [{
          title: 'Peliculas',
          icon: 'film-outline',
          url: 'Cliente/Peliculas'
        }];
        break;
      case 1:
        this.location = 'Administrador';
        this.labels = [{
          title: 'Peliculas',
          icon: 'film-outline',
          url: 'Administrador/Peliculas'
        }];
        break;
    }
  }
}
