import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FolderPageRoutingModule } from './folder-routing.module';

import { FolderPage } from './folder.page';

import { MoviesComponent as MoviesComponentAdmin } from './components/admin/movies/movies.component';

import { MoviesComponent as MoviesComponentClient } from './components/client/movies/movies.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FolderPageRoutingModule
  ],
  declarations: [
    FolderPage,
    MoviesComponentAdmin,
    MoviesComponentClient
  ]
})
export class FolderPageModule {}
