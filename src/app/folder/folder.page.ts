import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MoviesComponent as MoviesComponentAdmin } from './components/admin/movies/movies.component';

import { MoviesComponent as MoviesComponentClient } from './components/client/movies/movies.component';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit, AfterViewInit {

  @ViewChild(MoviesComponentAdmin) moviesAdmin: MoviesComponentAdmin;

  @ViewChild(MoviesComponentClient) moviesClient: MoviesComponentClient;

  public folder = {
    user: "",
    label: ""
  };

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.folder = {
      user: this.activatedRoute.snapshot.paramMap.get('user'),
      label: this.activatedRoute.snapshot.paramMap.get('label')
    };
  }

  ngAfterViewInit() {}

}
