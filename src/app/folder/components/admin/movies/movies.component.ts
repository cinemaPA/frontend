import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

import { DatabaseService } from '../../../../services/database/database.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-movies-admin',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss'],
})
export class MoviesComponent implements OnInit {

  public data = {
    idPelicula: '',
    nombre: '',
    duracion: '',
    rutaImagen: ''
  };

  public moviesSearch = [];
  public moviesAdded = [];

  constructor(
    private database: DatabaseService,
    private alertController: AlertController
    ) { }

    public async cargarPeliculas() {
      let data = await this.database.getMovies();
      this.moviesAdded = data.data;
    }

  public async presentAlert(subtitle: string, message: string) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Peliculas',
      subHeader: subtitle,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  public async presentAlertConfirm(movie: any) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Peliculas',
      message: 'Eliminar Pelicula ' + movie.nombre,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Okay',
          handler: () => {
            this.database.deleteMovie(movie)
            .then(data => {
              this.cargarPeliculas();
              this.presentAlert('Eliminar Peliculas', data.data);
            });
          }
        }
      ]
    });

    await alert.present();
  }

  ngOnInit() {
    this.cargarPeliculas();
  }

  public search(): void {
    this.moviesSearch = [];
    if(this.data.nombre && this.data.duracion) {
      this.database.searchMoviesFromApi(this.data)
      .then(moviesInfo => {
        moviesInfo.results.forEach(movie => this.moviesSearch.push({nombre: movie.title, idPelicula: movie.id, rutaImagen:  environment.imageMoviesApi + movie.poster_path, duracion: this.data.duracion}));
      });
    } else {
      this.presentAlert('Agregar Peliculas', 'Faltan datos');
    }
  }

  public select(movie: any): void {
    this.moviesSearch = [];
    this.data = {
      idPelicula: '',
      nombre: '',
      duracion: '',
      rutaImagen: ''
    };
    this.database.addMovie(movie)
    .then(data => {
      this.cargarPeliculas();
      this.presentAlert('Agregar Peliculas', data.data);
    })
    .catch(error => this.presentAlert('Agregar Peliculas', 'La pelicula ya existe entre las incluidas'));
  }

}
