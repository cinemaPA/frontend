import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';

import { DatabaseService } from '../../../../services/database/database.service';

@Component({
  selector: 'app-movies-client',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss'],
})
export class MoviesComponent implements OnInit {

  public movies = [];

  constructor(
    private actionSheetController: ActionSheetController,
    private database: DatabaseService
    ) {}

  async presentActionSheet(movie: any) {
    const actionSheet = await this.actionSheetController.create({
      header: movie.title,
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Titan',
        icon: 'business',
        handler: () => {
          console.log('Titan');
        }
      }, {
        text: 'Unicentro',
        icon: 'business',
        handler: () => {
          console.log('Unicentro');
        }
      }, {
        text: 'Plaza Central',
        icon: 'business',
        handler: () => {
          console.log('Plaza Central');
        }
      }, {
        text: 'Gran Estación',
        icon: 'business',
        handler: () => {
          console.log('Gran Estación');
        }
      }, {
        text: 'Embajador',
        icon: 'business',
        handler: () => {
          console.log('Embajador');
        }
      }, {
        text: 'Las Americas',
        icon: 'business',
        handler: () => {
          console.log('Las Americas');
        }
      }]
    });
    await actionSheet.present();
  }

  ngOnInit() {
    this.database.getMovies()
    .then(data => data.data.forEach(movie => {
      this.movies.push(movie);
    }));
  }

}
