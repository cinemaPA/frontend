import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  private header: HttpHeaders

  constructor(private http: HttpClient) {
    this.header = new HttpHeaders({
      'Authorization': `Bearer ${environment.token}`,
      'Content-Type': 'application/json;charset=utf-8'
    });
  }

  public searchMoviesFromApi(movie: any): Promise<any> {
    return new Promise((resolve, rejected) => {
      this.http.get(`${environment.moviesApi}search/movie?query=${movie.nombre}&language=es`, {headers: this.header}).toPromise()
      .then(data => resolve(data))
      .catch(error => rejected(error));
    });
  }

  public getMovies(): Promise<any> {
    return new Promise((resolve, rejected) => {
      this.http.get(`${environment.backendHost}pelicula/list`).toPromise()
      .then(data => resolve(data))
      .catch(error => rejected(error));
    });
  }

  public addMovie(movie: any): Promise<any> {
    movie.nombre = `'${movie.nombre}'`
    movie.rutaImagen = `'${movie.rutaImagen}'`
    return new Promise((resolve, rejected) => {
      this.http.post(`${environment.backendHost}pelicula/create`, movie).toPromise()
      .then(data => resolve(data))
      .catch(error => rejected(error));
    });
  }

  public deleteMovie(movie: any): Promise<any> {
    return new Promise((resolve, rejected) => {
      this.http.delete(`${environment.backendHost}pelicula?idPelicula=${movie.idPelicula}`).toPromise()
      .then(data => resolve(data))
      .catch(error => rejected(error));
    });
  }

}
